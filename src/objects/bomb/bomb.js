import { Container, Sprite, Texture } from "pixi.js";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { gameConstants } from "../../gameConstants";
import { Collider, ColliderEvent } from "../collider/collider";

export class Bomb extends Container {
  constructor() {
    super();
    this.bombs = [];
    this.dt = 0;
  }

  create() {
    let texture = Texture.from("../../../assets/bomb.png");
    this.bomb = new Sprite(texture);  
    this.addChild(this.bomb);

    this.config = {
      width : 30,
      height : 30,
      name : "bomb"
    }
    this.collider = new Collider();
    this.collider.create(this.config);
    this.collider.visible = gameConstants.DEBUG_COLLIDER;
    this.bomb.addChild(this.collider);
    CollisionDetector.addTag2(this.collider);
    return this.bomb; 
  }

  spawn(x, y){
    let bomb = this.create();
    bomb.damage = 50;
    bomb.x = x;
    bomb.y = y;
    return bomb;
  }

  randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}