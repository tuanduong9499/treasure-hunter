import { Container, Sprite, Texture } from "pixi.js";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { gameConstants } from "../../gameConstants";
import { Collider } from "../collider/collider";

export class Enemy extends Container {
  constructor(){
    super();
  }

  create(enemyX, enemyY){
    let texture = Texture.from("../../../assets/blob.png");
    this.enemy = new Sprite(texture);
    this.enemy.x = enemyX;
    this.enemy.y = enemyY;
    this.enemy.velocityY = 1;
    this.addChild(this.enemy);

    this.config = {
      width : 32,
      height : 22,
      name : "enemy"
    }
    let collider = new Collider();
    collider.create(this.config);
    collider.visible = gameConstants.DEBUG_COLLIDER;
    this.enemy.addChild(collider)

    CollisionDetector.addTag2(collider);
    return this.enemy;
  }

}