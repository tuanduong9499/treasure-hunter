import { Container } from "pixi.js";
import { gameConstants } from "../../gameConstants";
import { Enemy } from "./enemy";

export class EnemyController extends Container {
  constructor(){
    super();
    this.enemies = [];
    this.dt = 0;
    this.createEnemies();
  }

  createEnemies(){
    this.enemy = new Enemy;
    this.addChild(this.enemy);
    for(let i = 0; i < 5; i++){
      let enemyX = 60 + i * 100;
      let enemyY = this.randomInt(30, gameConstants.HEIGHT - 100);
      let enemy = this.enemy.create(enemyX, enemyY);
      this.enemies.push(enemy);
    }
  }

  move(){
    this.enemies.forEach((enemy) => {
      enemy.y += enemy.velocityY;
      if(enemy.y >= gameConstants.HEIGHT - 50 || enemy.y <= 30){
        enemy.velocityY *= -1;
      }
    })
  }

  randomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  update(dt){
    this.move();
  }
}