import { Container, Graphics, Sprite, Texture } from "pixi.js";

export class HealthBar extends Container {
  constructor(){
    super();
  }

  create(){
    let texture = Texture.from("../../../assets/healthBar.png");
    this.healthBar = new Sprite(texture);
    this.healthBar.width = 200;
    this.addChild(this.healthBar);
  }
}

export const configDefaultHealthBar = Object.freeze({
  x : 300,
  y : 10,
  width : 200,
  height : 20
})