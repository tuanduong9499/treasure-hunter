import { Container, Sprite, Texture } from "pixi.js";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { gameConstants } from "../../gameConstants";
import { Collider, ColliderEvent } from "../collider/collider";
import { HealthBar } from "./healthBar";

export class Player extends Container {
  constructor(parent) {
    super();
    this.velocityX = 5;
    this.velocityY = 5;
    this.create();
    this.move();
    this.initHealthBar(parent);
  }

  initHealthBar(parent) {
    
    this.healthBar = new HealthBar();
    this.healthBar.create();
    this.healthBar.x = 300;
    parent.addChild(this.healthBar);
  }

  create() {
    this.config = {
      width: 20,
      height: 30,
      name : "player"
    };

    let texture = Texture.from("../../../assets/explorer.png");
    this.player = new Sprite(texture);
    this.addChild(this.player);

    this.collider = new Collider();
    this.collider.create(this.config);
    this.collider.visible = gameConstants.DEBUG_COLLIDER;
    this.player.addChild(this.collider);

    CollisionDetector.addTag1(this.collider);
  }

  move() {
    document.addEventListener("keydown", (e) => {
      if (e.key == "ArrowRight") {
        this.x += this.velocityX;
        if(this.x >= gameConstants.WIDTH - 50){
          this.x = gameConstants.WIDTH - 50
        }
      }
      if (e.key == "ArrowLeft") {
        this.x -= this.velocityX;
        if(this.x <= 30){
          this.x = 30;
        }
      }
      if (e.key == "ArrowUp") {
        this.y -= this.velocityY;
        if(this.y < 0){
          this.y = 0;
        }
      }
      if (e.key == "ArrowDown") {
        this.y += this.velocityY;
        if(this.y >= gameConstants.HEIGHT - 50){
          this.y = gameConstants.HEIGHT - 50;
        }
      }
    });
  }
}