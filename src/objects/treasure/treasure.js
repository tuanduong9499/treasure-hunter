import { Container, Sprite, Texture } from "pixi.js";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { gameConstants } from "../../gameConstants";
import { Collider, ColliderEvent } from "../collider/collider";
import { Player } from "../player/player";

export class Treasure extends Container {
  constructor() {
    super();
  }

  create(){
    let texture = Texture.from("../../../assets/treasure.png");
    this.treasure = new Sprite(texture);
    this.addChild(this.treasure);

    this.config = {
      width : 30,
      height : 30,
      name : "treasure"
    }
    this.collider = new Collider();
    this.collider.create(this.config);
    this.collider.visible = gameConstants.DEBUG_COLLIDER;
    this.treasure.addChild(this.collider);

    CollisionDetector.addTag2(this.collider);
  }
}