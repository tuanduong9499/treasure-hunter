import { Container, Sprite, Texture } from "pixi.js";
import { CollisionDetector } from "../../collisionDetector/collisionDetector";
import { gameConstants } from "../../gameConstants";
import { Collider, ColliderEvent } from "../collider/collider";

export class Door extends Container {
  constructor() {
    super();
  }

  create(){
    let texture = Texture.from("../../../assets/door.png");
    this.door = new Sprite(texture);
    this.addChild(this.door);

    this.config = {
      width : 30,
      height : 30,
      name : "door"
    }
    this.collider = new Collider();
    this.collider.create(this.config);
    this.collider.visible = gameConstants.DEBUG_COLLIDER;
    this.door.addChild(this.collider);

    CollisionDetector.addTag3(this.collider);
  }
}