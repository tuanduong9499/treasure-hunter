import { Howl } from "howler";
import { Container, Sprite, Texture } from "pixi.js";
import { gameConstants } from "../gameConstants";
import { Bomb } from "../objects/bomb/bomb";
import { ColliderEvent } from "../objects/collider/collider";
import { Door } from "../objects/door/door";
import { EnemyController } from "../objects/enemy/enemyController";
import { Player } from "../objects/player/player";
import { Treasure } from "../objects/treasure/treasure";
import { GameOverScene } from "./gameOverScene";
import { GameVictoryScene } from "./gameVictoryScene";

export class PlayScene extends Container {
  constructor() {
    super();
    this.dt = 0;
    this.gamePlayContainer = new Container();
    this.isWin = false;
    this.addChild(this.gamePlayContainer);
    this.initGamePlay();
    this.initScenes();

    this.soundPlayGame = new Howl({
      src: ["../../assets/audioPlayGame.mp3"],
      html5: true,
      loop : true
    });

    this.soundGameOver = new Howl({
      src: ["../../assets/audioGameOver.mp3"],
    });

    this.soundGameVictory = new Howl({
      src: ["../../assets/audioGameVictory.mp3"],
    });

    this.soundPlayGame.play();
    
  }

  initGamePlay() {
    let texture = Texture.from("../../assets/dungeon.png");
    this.dungeon = new Sprite(texture);
    this.gamePlayContainer.addChild(this.dungeon);

    this.initPlayer();
    this.initEnemy();
    this.initDoor();
    this.initTreasure();
    this.initBomb();
  }

  initScenes() {
    this.initGameVictory();
    this.initGameOver()
  }

  initPlayer() {
    this.player = new Player(this.gamePlayContainer);
    this.player.x = 30;
    this.player.y = 50;
    this.gamePlayContainer.addChild(this.player);
  }

  initEnemy() {
    this.enemies = new EnemyController();
    this.gamePlayContainer.addChild(this.enemies);
    this.enemies.enemies.forEach((enemy) => {
      enemy.children[0].on(ColliderEvent.onCollider, (collider) => {
        collider = enemy.children[0].children[0].name;
        if (collider == "enemy") {
          this.player.healthBar.width -= 2;
          if (this.player.healthBar.width <= 0) {
            this.player.collider.enable = false;
            this.soundPlayGame.stop();
            this.soundGameOver.play();
            this.gamePlayContainer.visible = false;
            this.gameOver.visible = true;
          }
        }
      })
      
    })
  }

  initDoor() {
    if(!this.isWin){
      this.door = new Door();
      this.door.create();
      this.door.x = 30;
      this.gamePlayContainer.addChild(this.door);
      this.door.collider.on(ColliderEvent.onCollider, (collider) => {
        if(this.isWin){
          return;
        }
        this.isWin = true;
        collider = this.door.collider.children[0].name;
        if (collider === "door") {
          this.door.collider.enable = false;
          this.gamePlayContainer.visible = false;
          this.gameVictory.visible = true;
          this.soundPlayGame.stop();
          this.soundGameVictory.play();
        }
      });
      
    }
  }

  initTreasure() {
    this.treasure = new Treasure();
    this.treasure.create();
    this.treasure.x = gameConstants.WIDTH - 50;
    this.treasure.y = gameConstants.HEIGHT / 2;
    this.gamePlayContainer.addChild(this.treasure);

    this.treasure.collider.on(ColliderEvent.onCollider, (collider) => {
      collider = this.treasure.collider.children[0].name;
      if(collider == "treasure"){
        this.treasure.x = this.player.x + 8;
        this.treasure.y = this.player.y + 8;
      }
      
    });
  }

  initBomb(){
    this.bomb = new Bomb();
    this.gamePlayContainer.addChild(this.bomb);
  }

  initGameVictory() {
    this.gameVictory = new GameVictoryScene();
    this.gameVictory.visible = false;
    this.addChild(this.gameVictory);
  }

  initGameOver() {
    this.gameOver = new GameOverScene();
    this.gameOver.visible = false;
    this.addChild(this.gameOver);
  }

  update(dt) {
    this.enemies.update(dt);
    this.dt += dt;
    if (Math.round(this.dt) % 200 == 0) {
      let positionX = this.bomb.randomInt(50, gameConstants.WIDTH - 50);
      let positionY = this.bomb.randomInt(50, gameConstants.HEIGHT - 50);
      let bomb = this.bomb.spawn(positionX, positionY)
      this.bomb.bombs.push(bomb);

      let colliderBomb = bomb.children[0];
      colliderBomb.on(ColliderEvent.onCollider, (collider) => {
        collider = colliderBomb.children[0].name;
        if (collider == "bomb"){
          console.log("bomb")
          colliderBomb.enable = false;
          this.player.healthBar.width -= bomb.damage;
          if (this.player.healthBar.width <= 0) {
            this.player.collider.enable = false;
            this.gamePlayContainer.visible = false;
            this.gameOver.visible = true;
            this.soundPlayGame.stop();
            this.soundGameOver.play();
          }
        } 
      })
    }

    //destroy bomb
    if (this.bomb.bombs.length >= 2) {
      let bombDelete = this.bomb.bombs.shift();
      bombDelete.destroy();
    }
  }
}