import { Container, Sprite, Text, TextStyle, Texture } from "pixi.js";
import { gameConstants } from "../gameConstants";

export class GameOverScene extends Container {
  constructor() {
    super();
    this.create();
    this.buttonNewGame();
  }

  create() {
    let textStyle = new TextStyle({
      fontSize: 48,
      fill: "0xffffff",
    });
    let text = new Text("GAME OVER", textStyle);
    text.anchor.set(0.5);
    text.x = gameConstants.WIDTH / 2;
    text.y = gameConstants.HEIGHT / 2;
    this.addChild(text);
  }

  buttonNewGame() {
    let texture = Texture.from("../../assets/button.png");
    this.button = new Sprite(texture);
    this.button.x = gameConstants.WIDTH / 2;
    this.button.y = gameConstants.HEIGHT / 2 + 100;
    this.button.anchor.set(0.5);
    this.addChild(this.button);

    let textStyle = new TextStyle({
      fill: "0xffffff",
    });
    let textButton = new Text("NEW GAME", textStyle);
    textButton.anchor.set(0.5);
    this.button.addChild(textButton);
    this.button.interactive = true;
    this.button.on("pointerdown", () => {
      location.reload();
    });
  }
}
